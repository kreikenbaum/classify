#! /bin/sh
## exports mongodb data to TARGET, removes from local db
TARGET=~/.public_html/data/$(date +%F)
mkdir -p $TARGET
mongoexport -d sacred -c runs >> $TARGET/$(hostname)-dump-runs
mongoexport -d sacred -c fs.files >> $TARGET/$(hostname)-dump-files
mongoexport -d sacred -c fs.chunks >> $TARGET/$(hostname)-dump-chunks
mongo sacred --eval "printjson(db.dropDatabase())"
