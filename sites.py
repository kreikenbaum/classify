'''check whether a site in in the top... sites'''
import logging

import config
import logconfig

cache = []
bg = set()
top = set()


def _init():
    global bg, cache, top
    if not cache:
        with open(config.SITES) as f:
            for line in f:
                cache.append(line.rstrip().split(',')[1])
    top = set(cache[:100])
    bg = set(cache[100:])


def top_100(site):
    '''@return if site is in top 100 sites'''
    return site in top


def background(trace):
    '''@return if trace's domain in sites, but not in top_100'''
    return trace.domain in bg and not top_100(trace.domain)


def clean(trace_dict):
    '''@return trace_dict with only sites and bg pages in capture list'''
    out = {}
    for (site, traces) in trace_dict.iteritems():
        if site == "background":
            out['background'] = []
            for trace in traces:
                if background(trace):
                    out['background'].append(trace)
                else:
                    logging.warn("filtered %s", trace.name)
        elif top_100(site):
            out[site] = trace_dict[site]
        else:
            logging.info("filtered %s", site)
    return out


_init()
